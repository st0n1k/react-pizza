import React from 'react';
import classnames from 'classnames';

const onButtonClick = (name) => {};

const Button = ({ className, outline, children }) => {
  return (
    <button
      className={classnames('button', className, {
        'button--outline': outline,
      })}
      onClick={() => onButtonClick(children)}>
      {children}
    </button>
  );
};

export default Button;
